# proposal "Percentage-of-dollar-goal"

1. Each project specifies a dollars per month funding Goal (G). 
1. Each patron of a project specifies the Max amount (M) they would be willing to donate per month to that project as part of a crowd that was collectively meeting the project's funding goal.
1. Each month the percentage of the dollar goal (P) is calculated as (Sum of M across all patrons / G)
1. If P<100, each patron's actual donation (D) equals D=(P/100)xM
1. If P>=100, each patron's actual donation (D) equals D=M
