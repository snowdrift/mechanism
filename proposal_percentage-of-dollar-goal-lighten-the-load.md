This is a variation on proposal_percentage-of-dollar-goal. It is identical except that:

1. If P>=100, each patron's actual donation (D) equals D=M/(P/100)

In this variation, the project never receives more than their goal, and patrons never donate more than is necessary for the crowd to exactly meet the project's goal.

A sub-variation of this variation would be to give each patron the choice of what happens after the goal is reached: Either they keep donating their max D=M, or they donate just enough to contribute to the crowd meeting (but not to exceeding) the goal, D=M/(P/100).
